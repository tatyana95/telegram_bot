from my_bot.enums import ChatWaitingStatus
from django.db.transaction import atomic

from my_bot import helpers

def on_start(chat_id, params=None):

	return helpers.hello_text, {'reply_markup': helpers.keyboard}
		
@atomic
def on_text(chat_id, text):

	if text == 'О ППОС КФУ':
		message = 'Первичная профсоюзная организация студентов КФУ (ППОС КФУ) создана на конференции студенческого коллектива университета 12 ноября 1987 года. '
	elif text == 'ЛИЦА ППОС КФУ':
		message = 'Выберите пункт меню 👇'
		return message, {'reply_markup': helpers.keyboard_faces}
	elif text == 'БЛАНКИ ДОКУМЕНТОВ':
		message = 'Бланки доков и заявлений'
	elif text == 'ПОЛЕЗНЫЕ ССЫЛКИ':
		message = 'Полезные ссылки'
	elif text == 'КОНТАКТЫ ППОС':
		message = helpers.contacts
	elif text == 'Аппарат ППОС':
		return helpers.apparat, {'reply_markup': helpers.keyboard_faces}
	elif text == 'Председатели профбюро институтов/ факультетов':
		return helpers.predsed, {'reply_markup': helpers.keyboard_faces}
	elif text == 'Председатели комиссий':
		return 'Председатели комиссий', {'reply_markup': helpers.keyboard_faces}
	elif text == 'Руководители центров':
		return 'Руководители центров', {'reply_markup': helpers.keyboard_faces}
	elif text == 'Go Back':
		message = 'Главное меню'
	else:
		message = helpers.hello_text
	
	return message, {'reply_markup': helpers.keyboard}
       