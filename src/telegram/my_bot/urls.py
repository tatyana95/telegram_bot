from django.conf.urls import url
from .views import TelegramView


urlpatterns = [
    url(r'^bot/(?P<token>.+)/$', TelegramView.as_view()),
]