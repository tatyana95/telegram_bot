from django_enumfield import enum


class ChatWaitingStatus(enum.Enum):
    START = 1
    STOP = 2
    NONE = 3