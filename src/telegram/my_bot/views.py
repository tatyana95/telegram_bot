import json

from django.views.generic import View
from django.http import HttpResponseForbidden, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.conf import settings

import telepot
import urllib3

from my_bot import handlers
from my_bot.helpers import process_response

if settings.PYTHON_ANYWHERE:
    proxy_url = "http://proxy.server:3128"
    telepot.api._pools = {
        'default': urllib3.ProxyManager(
            proxy_url=proxy_url, num_pools=3, maxsize=10, retries=False, timeout=30
        ),
    }
    telepot.api._onetime_pool_spec = (
        urllib3.ProxyManager, 
        dict(proxy_url=proxy_url, num_pools=1, maxsize=1, retries=False, timeout=30)
    )


TelegramBot = telepot.Bot(settings.TELEGRAM_BOT_TOKEN)

Commands = {
    '/start': handlers.on_start,
    '/on': handlers.on_start,
}

class TelegramView(View):

    def post(self, request, token):
        if token != settings.TELEGRAM_BOT_TOKEN:
            return HttpResponseForbidden('Invalid token')

        try:
            data = json.loads(request.body.decode('utf-8'))
        except ValueError:
            return HttpResponseBadRequest('Invalid request body')

        message = data['message']
        f, g = telepot.flance(message)
        content_type, chat_type, chat_id = g

        if f == 'chat' and content_type == 'text':
            text = message['text']
            response = ''
            if text.startswith('/'):
                command, *params = text.split()
                handler = Commands.get(command)
                if handler:
                    response = handler(chat_id, params)
            else:
               response = handlers.on_text(chat_id, text)

            text, extra = process_response(response)

            if text or extra:
                TelegramBot.sendMessage(chat_id, text, **extra)

        return JsonResponse({}, status=200)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)